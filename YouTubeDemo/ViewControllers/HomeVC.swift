//
//  HomeVC.swift
//  YouTubeDemo
//
//  Created by Dhruvit on 26/06/17.
//  Copyright © 2017 InheritX. All rights reserved.
//

import UIKit

enum HomeControllerType {
    case home
    case trending
    case subscriptions
}

class HomeVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate  {
    
    //MARK: - Properties
    @IBOutlet weak var tableView: UITableView!
    var videos = [Video]()
    var lastContentOffset: CGFloat = 0.0
    
    var controllerType = HomeControllerType.home
    
    //MARK: Methods
    func customization() {
        self.tableView.contentInset = UIEdgeInsetsMake(50, 0, 30, 0)
        self.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(50, 0, 30, 0)
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 300
    }
    
    func fetchData() {
        
        // Form the request URL string.
//        var urlString = "https://www.googleapis.com/youtube/v3/search?part=snippet&q=a&type=video&key=\(kAPIKEY)"
//
//        switch controllerType {
//        case .home:
//            print("home")
//            urlString = "https://www.googleapis.com/youtube/v3/search?part=snippet&q=a&type=video&key=\(kAPIKEY)"
//
//
//        case .trending:
//            print("trending")
//            urlString = "https://www.googleapis.com/youtube/v3/search?part=snippet&type=video&chart=mostPopular&key=\(kAPIKEY)"
//
//        case .subscriptions:
//            print("subscriptions")
//            urlString = "https://www.googleapis.com/youtube/v3/search?part=snippet&type=channels&key=\(kAPIKEY)"
//        }
        
//        APICallManager.sharedInstance.getVideoList(urlString: urlString)
        
        Video.fetchVideos { [weak self] response in
            guard let weakSelf = self else {
                return
            }
            weakSelf.videos = response
            weakSelf.videos.shuffle()
            weakSelf.tableView.reloadData()
        }
    }
    
    //MARK: Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.videos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VideoCell") as! VideoCell
        cell.set(video: self.videos[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        NotificationCenter.default.post(name: NSNotification.Name("open"), object: nil)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (self.lastContentOffset > scrollView.contentOffset.y) {
            NotificationCenter.default.post(name: NSNotification.Name("hide"), object: false)
        } else {
            NotificationCenter.default.post(name: NSNotification.Name("hide"), object: true)
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        self.lastContentOffset = scrollView.contentOffset.y;
    }
    
    //MARK: -  ViewController Lifecylce
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.customization()
        
        self.controllerType = HomeControllerType.home
        
        self.fetchData()
    }
    
}

