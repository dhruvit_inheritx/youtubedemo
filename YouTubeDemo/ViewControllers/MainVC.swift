//
//  MainVC.swift
//  YouTubeDemo
//
//  Created by Dhruvit on 26/06/17.
//  Copyright © 2017 InheritX. All rights reserved.
//

import UIKit

class MainVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    //MARK: Properties
    @IBOutlet var tabBarView: TabBarView!
    @IBOutlet weak var collectionView: UICollectionView!
    var views = [UIView]()
    
    //MARK: Methods
    func customization()  {
        self.view.backgroundColor = UIColor.rbg(r: 228, g: 34, b: 24)
        
        //CollectionView Setup
        self.collectionView.contentInset = UIEdgeInsetsMake(44, 0, 0, 0)
//        self.collectionView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: (self.view.frame.size.height - 88))
//        self.collectionView.layer.borderWidth = 1.0
//        self.collectionView.layer.borderColor = UIColor.yellow.cgColor
        
        //TabbarView setup
        self.view.addSubview(self.tabBarView)
        self.tabBarView.translatesAutoresizingMaskIntoConstraints = false
        let _ = NSLayoutConstraint.init(item: self.view, attribute: .top, relatedBy: .equal, toItem: self.tabBarView, attribute: .top, multiplier: 1.0, constant: 0).isActive = true
        let _ = NSLayoutConstraint.init(item: self.view, attribute: .left, relatedBy: .equal, toItem: self.tabBarView, attribute: .left, multiplier: 1.0, constant: 0).isActive = true
        let _ = NSLayoutConstraint.init(item: self.view, attribute: .right, relatedBy: .equal, toItem: self.tabBarView, attribute: .right, multiplier: 1.0, constant: 0).isActive = true
        self.tabBarView.heightAnchor.constraint(equalToConstant: 64).isActive = true
        
        //ViewController init
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC")
        let trendingVC = self.storyboard?.instantiateViewController(withIdentifier: "TrendingVC")
        let subscriptionsVC = self.storyboard?.instantiateViewController(withIdentifier: "SubscriptionsVC")
        let accountVC = self.storyboard?.instantiateViewController(withIdentifier: "AccountVC")
        
        let viewControllers = [homeVC, trendingVC, subscriptionsVC, accountVC]
        for vc in viewControllers {
            self.addChildViewController(vc!)
            vc!.didMove(toParentViewController: self)
            vc!.view.frame = CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: (UIScreen.main.bounds.size.height - 88))
//            vc?.view.layer.borderColor = UIColor.blue.cgColor
//            vc?.view.layer.borderWidth = 3.0
//            vc?.view.layoutIfNeeded()
            self.views.append(vc!.view)
        }
        self.collectionView.reloadData()
        
        //NotificationCenter setup
        NotificationCenter.default.addObserver(self, selector: #selector(self.scrollViews(notification:)), name: Notification.Name.init(rawValue: "didSelectMenu"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.hideBar(notification:)), name: NSNotification.Name("hide"), object: nil)
        
        self.view.layoutIfNeeded()
    }
    
    @objc func scrollViews(notification: Notification) {
        if let info = notification.userInfo {
            let userInfo = info as! [String: Int]
            self.collectionView.scrollToItem(at: IndexPath.init(row: userInfo["index"]!, section: 0), at: .centeredHorizontally, animated: true)
        }
    }
    
    @objc func hideBar(notification: NSNotification) {
//        let state = notification.object as! Bool
//        self.navigationController?.setNavigationBarHidden(state, animated: true)
//        self.view.layoutIfNeeded()
    }
    
    //MARK: Delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.views.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        let vcView = self.views[indexPath.row]
        cell.contentView.addSubview(vcView)
        
        vcView.translatesAutoresizingMaskIntoConstraints = false
        let _ = NSLayoutConstraint.init(item: cell.contentView, attribute: .top, relatedBy: .equal, toItem: vcView, attribute: .top, multiplier: 1.0, constant: 0).isActive = true
        let _ = NSLayoutConstraint.init(item: cell.contentView, attribute: .left, relatedBy: .equal, toItem: vcView, attribute: .left, multiplier: 1.0, constant: 0).isActive = true
        let _ = NSLayoutConstraint.init(item: cell.contentView, attribute: .right, relatedBy: .equal, toItem: vcView, attribute: .right, multiplier: 1.0, constant: 0).isActive = true
        let _ = NSLayoutConstraint.init(item: cell.contentView, attribute: .bottom, relatedBy: .equal, toItem: vcView, attribute: .bottom, multiplier: 1.0, constant: 0).isActive = true
        
//        cell.clipsToBounds = true
//        cell.contentView.layer.borderColor = UIColor.cyan.cgColor
//        cell.contentView.layer.borderWidth = 3.0
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize.init(width: self.collectionView.bounds.width, height: (self.collectionView.bounds.height + 22))
        return CGSize.init(width: self.collectionView.frame.size.width, height: self.collectionView.frame.size.height)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
       let scrollIndex = scrollView.contentOffset.x / self.view.bounds.width
        NotificationCenter.default.post(name: Notification.Name.init(rawValue: "scrollMenu"), object: nil, userInfo: ["length": scrollIndex])
    }
    
    //MARK: ViewController lifecyle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.customization()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}
