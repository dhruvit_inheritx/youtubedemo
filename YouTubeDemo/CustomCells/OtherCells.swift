//
//  OtherCells.swift
//  YouTubeDemo
//
//  Created by Dhruvit on 27/06/17.
//  Copyright © 2017 InheritX. All rights reserved.
//

import Foundation
import UIKit

class videoCell: UITableViewCell {
    
    @IBOutlet weak var tumbnail: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var name: UILabel!
}

class SearchCell: UITableViewCell {
    @IBOutlet weak var resultLabel: UILabel!
}

//TabBarCell Class
class TabBarCellCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var icon: UIImageView!
}
