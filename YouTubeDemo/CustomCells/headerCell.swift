//
//  headerCell.swift
//  YouTubeDemo
//
//  Created by Dhruvit on 27/06/17.
//  Copyright © 2017 InheritX. All rights reserved.
//

import UIKit

class headerCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var viewCount: UILabel!
    @IBOutlet weak var likes: UILabel!
    @IBOutlet weak var disLikes: UILabel!
    @IBOutlet weak var channelTitle: UILabel!
    @IBOutlet weak var channelPic: UIImageView!
    @IBOutlet weak var channelSubscribers: UILabel!
    
}
