//
//  APIRouter.swift
//  YouTubeDemo
//
//  Created by Dhruvit on 27/06/17.
//  Copyright © 2017 InheritX. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration

class APICallManager: NSObject {
    
    static let sharedInstance: APICallManager = {
        let instance = APICallManager()
        return instance
    }()
    
    func getVideoList(urlString : String) {
        
        // Create a NSURL object based on the above string.
        let url = URL.init(string: urlString)
        
        let _ = URLSession.shared.dataTask(with: url!) { (data, response, error) in
            
            if error == nil {
                if let json = try? JSONSerialization.jsonObject(with: data!, options: .mutableContainers) {
                    let data = json as! [String:Any]
                    let arrayItems = data["items"] as! [Any]
                    
                    print("data :- ",arrayItems[0])
                    
                }
            }
        }
        
        //    let _  = URLSession.shared.dataTask(with: url!, completionHandler: { [weak self] (data, response, error) in
        //        guard let weakSelf = self else {
        //            return
        //        }
        //
        //    }).resume()
    }
    
    func isConnectedToNetwork() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
}
